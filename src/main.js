import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'

Vue.config.productionTip = false

import axios from 'axios'
import Notifications from 'vue-notification'
import VueGoodTablePlugin from 'vue-good-table';

import 'vue-good-table/dist/vue-good-table.css'

Vue.use(Vuex);
Vue.use(Notifications);
Vue.use(VueGoodTablePlugin);

new Vue({
  router,
  axios,
  render: h => h(App)
}).$mount('#app')
