import axios from 'axios'
const BaseConnection = axios.create({
    baseURL: `https://websecurity.kirigokaranja.com/`,
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  })


  export default BaseConnection